#include <iostream>

template<typename T>
class List {
    public:
        List(T d);
        ~List();
        void append(T d);
        T head();
        List<T> tail();

    private:
        void inc_ref();
        void dec_ref();
        int get_ref();

        T data;
        List* next;
        List* prev;
        int rc;
};

template<typename T>
List<T>::List(T d) : data{d}, next{nullptr}, prev{nullptr}, rc{1} {  }

template<typename T>
List<T>::~List() {
    if(next) {
        if(next->get_ref() > 1) {
            next->dec_ref();
        }
        else {
            delete next;
        }
    }
}

template<typename T>
void List<T>::append(T d) {
    if(next) {
        next->append(d);
    }
    else {
        next = new List<T>(d);
        next->prev = this;
        next->rc = rc;
    }
}

template<typename T>
T List<T>::head() {
    return data;
}

template<typename T>
List<T> List<T>::tail() {
    if (next) {
        auto n = *next;
        n.prev = nullptr;
        if (n.next) {
            n.next->inc_ref();
        }
        return n;
    }
    else {
        throw std::out_of_range("End of list");
    }
}

template<typename T>
int List<T>::get_ref() {
    return rc;
}

template<typename T>
void List<T>::inc_ref() {
    ++rc;
    if (next) {
        next->inc_ref();
    }
}
template<typename T>
void List<T>::dec_ref() {
    --rc;
    if (next) {
        next->dec_ref();
    }
}
