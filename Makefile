CXX=clang++
CXXFLAGS=-std=c++14 -Wall

all: bin_dir program

program:
	$(CXX) $(CXXFLAGS) src/main.cpp -o bin/list -Isrc

bin_dir:
	mkdir -p bin
